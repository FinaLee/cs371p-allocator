// ----------------------------------
// projects/c++/allocator/Allocator.h
// Copyright (C) 2018
// Glenn P. Downing
// ----------------------------------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <iostream> // cout, endl, getline

using namespace std;

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * @return a bool if the memory blocks are valid
     */
    bool valid () const {
        int currIndex = 0;
        bool prevPositive = false;
        while(currIndex < (int)N) {
            int size = (*this)[currIndex];
            int absSize = (size<0) ? -size : size;
            if (size != (*this)[currIndex+absSize+4]) {
                return false;
            }
            bool thisPositive = (size>0);
            if(prevPositive && thisPositive) {
                return false;
            }
            prevPositive = thisPositive;

            if(size<0)
                size*=-1;
            currIndex += (size+8);
        }
        return true;
    }

    // -------------
    // coalesce_right
    // -------------

    /**
     * O(1) in space
     * O(1) in time
     * @param index the index of the block to deallocate
     * @param size the size of the block to deallocate
     */
    void coalesce_right(int index, int size) {
        if(index+size+4 != (int)N) {
            int rightSize = (*this)[index+size+4];
            if (rightSize>0) {
                int newSize = size+rightSize+8;
                (*this)[index-4] = newSize;
                (*this)[index+newSize] = newSize;
                return;
            }
        }
        (*this)[index-4] = size;
        (*this)[index+size] = size;
    }

public:
    // ------------
    // constructors
    // ------------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        size_type n = (size_type)N;
        if(n < sizeof(T) + 8)
            throw bad_alloc();
        (*this)[0] = n-8;
        (*this)[n-4] = n-8;
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;


    // -------
    // get_mem
    // -------

    /**
     * @return a pointer to the memory
     */
    char* get_mem() {
        return a;
    }

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     * @param n the number of objects to allocate
     * @return a pointer to the allocated block
     */
    pointer allocate (size_type n) {
        int minSize = sizeof(T) + (2 * sizeof(int));
        int bytes = sizeof(T)*n;
        int currIndex = 0;
        int newSize;
        while(currIndex < (int)N) {
            int size = (*this)[currIndex];
            if(size > 0 && size >= bytes) {
                newSize = size - bytes;
                // if there would be less than the min size left, give it the whole block.
                if(newSize < minSize) {
                    int currBlock = currIndex;
                    (*this)[currIndex] = (-1*size);
                    currIndex += size+4;
                    (*this)[currIndex] = (-1*size);
                    assert(valid());
                    return (T*)(a+currBlock+4);
                }
                // else if there is enough room for multiple blocks, split the block add a new sentinel.
                int currBlock = currIndex;
                (*this)[currIndex] = (-1*bytes);
                currIndex += (bytes+4);
                (*this)[currIndex] = (-1*bytes);
                currIndex += 4;
                // set the adjacent sentinel
                (*this)[currIndex] = (newSize-8);
                currIndex += (newSize-4);
                (*this)[currIndex]=(newSize-8);
                assert(valid());
                return (T*)(a+currBlock+4);
            }
            if(size < 0)
                size*=-1;
            currIndex += (8 + size);
        }
        // if we have made it here, there must be no space.
        throw bad_alloc();
    }



    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());                            // from the prohibition of new
    }



    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * @param block a pointer to the block to deallocate
     */
    void deallocate (pointer block, size_type) {
        if(block == nullptr)
            throw invalid_argument("Pointer is null. Cannot deallocate.");
        char* mem = (char*)block;
        int index = mem - a;
        if(index < 0 || index >= (int)N) {
            throw invalid_argument("Pointer is invalid.");
        }
        int size = (*this)[index-4];
        if(size > 0) {
            throw invalid_argument("Cannot deallocate a free block.");
        }
        size*=-1;
        coalesce_right(index, size);
        if(index - sizeof(int) != 0) {
            int leftSize = (*this)[index-8];
            if (leftSize>0) {
                coalesce_right(index-(8+leftSize), leftSize);
            }
        }
        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }
};


#endif // Allocator_h
