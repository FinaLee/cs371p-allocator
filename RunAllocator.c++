// ---------------------------------------
// projects/c++/allocator/RunAllocator.c++
// Copyright (C) 2018
// Glenn P. Downing
// ---------------------------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <fstream>  // ifstream
#include <iostream> // cout, endl, getline
#include <string>   // s
#include <vector>   // vector

#include "Allocator.h"
using namespace std;

vector<int> make_input(istream & r) {
    string s;
    vector<int> numsToAllocate;
    while(getline(r,s) && !s.empty()) {
        int numBytes = stoi(s);
        numsToAllocate.push_back(numBytes);
    }
    return numsToAllocate;
}

// returns a pointer to the sentinel of the correct busy block
int* help_deallocate(char* mem, int busyBlock) {
    int numBusyBlocks = 0;
    int currIndex = 0;
    while(currIndex < 1000) {
        int sentinel = *(int*)(mem);
        if(sentinel < 0) {
            ++numBusyBlocks;
            sentinel*=-1;
        }
        if(numBusyBlocks == busyBlock) {
            return (int*)(mem);
        }
        mem += (8 + sentinel);
        currIndex += (8 + sentinel);
    }

    return nullptr;
}

void print(char* mem) {
    int currIndex = 0;
    vector<int> blocks;
    while(currIndex < 1000) {
        int sentinel = *(int*)(mem);
        blocks.push_back(sentinel);
        if(sentinel < 0)
            sentinel*=-1;
        mem += (8 + sentinel);
        currIndex += (8 + sentinel);
    }
    for(int i = 0; i < (int)blocks.size()-1; i++) {
        cout<< blocks[i] << " ";
    }
    cout << blocks[blocks.size()-1] << endl;
}

int main () {
    string s;
    getline(cin, s);
    int numTests = stoi(s);
    assert(numTests > 0 && numTests < 100);
    getline(cin,s); // read first blank line
    for (int i =0; i < numTests; ++i) {
        vector<int> input = make_input(cin);
        my_allocator<double, 1000> al;
        for (int j = 0; j < (int)input.size(); ++j) {
            int in = input[j];
            if(in > 0 )
                al.allocate(in);
            else {
                int* block = help_deallocate(al.get_mem(), -1*in);
                if(block != nullptr) {
                    al.deallocate((double*)(block+1), (*block)/-8);
                }
            }
        }
        print(al.get_mem());
    }
    return 0;
}
