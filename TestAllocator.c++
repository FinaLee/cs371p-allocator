// -----------------------------------------
// projects/c++/allocator/TestAllocator1.c++
// Copyright (C) 2017
// Glenn P. Downing
// -----------------------------------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/Primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/AdvancedGuide.md

// --------
// includes
// --------

#include <algorithm> // count
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.h"

// --------------
// TestAllocator1
// --------------


template <typename A>
struct TestAllocator1 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
std::allocator<int>,
    std::allocator<double>,
    my_allocator<int,    100>,
    my_allocator<double, 100>>
    my_types_1;

TYPED_TEST_CASE(TestAllocator1, my_types_1);


TYPED_TEST(TestAllocator1, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type      s = 1;
    const value_type     v = 2;
    const pointer        p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator1, test_2) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type      s = 5;
    const value_type     v = 11;
    const pointer        p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator1, test_3) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type      s = 7;
    const value_type     v = 11;
    const value_type     k = 12;
    const pointer        p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        x.construct (p, k);
        ASSERT_EQ(k, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}
TYPED_TEST(TestAllocator1, test_4) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    allocator_type y;
    const size_type      s = 7;
    const value_type     v = 11;
    const value_type     k = 12;
    const pointer        p = x.allocate(s);
    const pointer        n = y.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        y.construct (n, k);
        ASSERT_NE(*n, *p);

        x.destroy(p);
        x.deallocate(p, s);
        y.destroy(n);
        y.deallocate(n, s);
    }
}


TYPED_TEST(TestAllocator1, test_5) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type  x;
    const size_type       s = 10;
    const value_type      v = 2;
    const pointer         b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
            --p;
            x.construct(p, (value_type) 7);
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ((s-1), std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}




TYPED_TEST(TestAllocator1, test_10) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type  x;
    const size_type       s = 10;
    const value_type      v = 2;
    const pointer         b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// --------------
// TestAllocator2
// --------------

TEST(TestAllocator2, const_index) {
    const my_allocator<int, 100> x;
    ASSERT_EQ(x[0], 92);
}



TEST(TestAllocator2, min_allocation) {
    bool caught = false;
    try {
        const my_allocator<int, 1> x;
    }
    catch(...) {

        caught = true;
    }
    ASSERT_EQ(caught, true);

}


TEST(TestAllocator2, index) {
    my_allocator<int, 100> x;
    ASSERT_EQ(x[0], 92);
}

TEST(TestAllocator2, indexEnd) {
    my_allocator<int, 100> x;
    ASSERT_EQ(x[96], 92);
}

TEST(TestAllocator2, indexDouble) {
    my_allocator<double, 1000> x;
    ASSERT_EQ(x[0], 992);
}

TEST(TestAllocator2, allocatedIndex) {
    my_allocator<double, 1000> x;
    x.allocate(3);
    ASSERT_EQ(x[0], -24);
}


TEST(TestAllocator2, coalesceTest) {
    my_allocator<double, 1000> x;
    double*  a = x.allocate(3);
    double*  b = x.allocate(4);
    x.allocate(3);
    x.deallocate(b, 4);
    x.deallocate(a,3); 
    ASSERT_EQ(x[0], 64);
}


TEST(TestAllocator2, allocateDeallocate) {
    my_allocator<double, 1000> x;
    double * p = x.allocate(3);
    x.deallocate(p, 3);
    ASSERT_EQ(x[0], 992);
}





// --------------
// TestAllocator3
// --------------

template <typename A>
struct TestAllocator3 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
my_allocator< int, 100>,
              my_allocator<double, 100>>
              my_types_2;

TYPED_TEST_CASE(TestAllocator3, my_types_2);


TYPED_TEST(TestAllocator3, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type      s = 1;
    const value_type     v = 2;
    const pointer        p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}


// pass dealocate a null pointer
TYPED_TEST(TestAllocator3, test_2) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type      s = 1;
    const value_type     v = 2;
    const pointer        p = x.allocate(s);
    bool caught = false;
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        try {
            x.deallocate(nullptr, s);
        }
        catch(...) {

            caught = true;

        }
        ASSERT_TRUE(caught);
    }
}

// pass a negative number to allocate
TYPED_TEST(TestAllocator3, test_3) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::size_type      size_type;

    allocator_type x;

    const size_type      s = 10000;
    bool caught = false;

    try {
        x.allocate(s);
    }
    catch(...) {

        caught = true;

    }
    ASSERT_TRUE(caught);
}


// pass in a pointer to another array
TYPED_TEST(TestAllocator3, test_4) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type      s = 1;
    const value_type     v = 2;
    const pointer        p = x.allocate(s);
    value_type *n = {};
    bool caught = false;

    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        try {
            x.deallocate(n, s);
        }
        catch (...) {

            caught = true;

        }

    }
    ASSERT_TRUE(caught);

}
// pass in a pointer to another array
TYPED_TEST(TestAllocator3, test_5) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type      s = 1;
    const value_type     v = 2;
    const pointer        p = x.allocate(s);
    bool caught = false;

    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);

        try {
            x.deallocate(p, s);

        }
        catch (...) {

            caught = true;

        }

    }
    ASSERT_TRUE(caught);

}
TYPED_TEST(TestAllocator3, test_10) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type      s = 10;
    const value_type     v = 2;
    const pointer        b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}
